<?php

/*
 * (c) Javier Eguiluz <javier.eguiluz@gmail.com>
 *
 * Este archivo pertenece a la aplicación de prueba Cupon.
 * El código fuente de la aplicación incluye un archivo llamado LICENSE
 * con toda la información sobre el copyright y la licencia.
 */

namespace Greibit\BioBundle\DataFixtures\MongoDB;

use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use Greibit\BioBundle\Document\Certification;
use Greibit\BioBundle\Document\Experience;
use Greibit\BioBundle\Document\Knowledge;
use Greibit\BioBundle\Document\Language;
use Greibit\BioBundle\Document\OtherInfo;
use Greibit\BioBundle\Document\Training;
use Greibit\BioBundle\Document\User;
use Symfony\Component\DependencyInjection\ContainerAwareInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Fixtures de la entidad Usuario.
 * Crea 500 usuarios de prueba con información muy realista.
 */
class Usuarios extends AbstractFixture implements OrderedFixtureInterface, ContainerAwareInterface
{
    public function getOrder()
    {
        return 40;
    }

    private $container;

    public function setContainer(ContainerInterface $container = null)
    {
        $this->container = $container;
    }

    public function load(ObjectManager $manager)
    {
        // Obtener todas las ciudades de la base de datos
        //$ciudades = $manager->getRepository('CiudadBundle:Ciudad')->findAll();

        for ($i=1; $i<=5; $i++) {
            $usuario = new User();

            $usuario->setName($this->getNombre());
            $usuario->setSurname($this->getApellidos());
            $usuario->setEmail('usuario'.$i.'@greibio.com');
            $usuario->setUsername('usuario'.$i);

            $usuario->setSalt(base_convert(sha1(uniqid(mt_rand(), true)), 16, 36));

            $passwordEnClaro = 'usuario'.$i;
            $encoder = $this->container->get('security.encoder_factory')->getEncoder($usuario);
            $passwordCodificado = $encoder->encodePassword($passwordEnClaro, $usuario->getSalt());
            $usuario->setPassword($passwordCodificado);

            $usuario->setRoles(array("ROLE_USER"));
            $usuario->setBirthday(new \DateTime('now - '.rand(7000, 20000).' days'));

            $usuario->setAddress($this->getDireccion());

            $usuario->setEnabled(true);
            for($k = 0 ; $k < $i ; $k++){
                $usuario->addKnowledge($this->getKnowledge());
            }

            for($k = 0 ; $k < $i ; $k++){
                $e = new Training();
                $e->setCenter($this->getText());
                $e->setTitle($this->getText());
                $e->setStartDate(new \DateTime('now - '.rand(7000, 20000).' days'));
                $e->setFinishDate(new \DateTime('now - '.rand(7000, 20000).' days'));
                $usuario->addTraining($e);
            }

            for($k = 0 ; $k < $i ; $k++){
                $e = new Experience();
                $e->setCompany($this->getText());
                $e->setProject($this->getText());
                $e->setWork($this->getText());
                $e->setStartDate(new \DateTime('now - '.rand(7000, 20000).' days'));
                $e->setFinishDate(new \DateTime('now - '.rand(7000, 20000).' days'));
                $usuario->addExperience($e);
            }

            for($k = 0 ; $k < $i ; $k++){
                $e = new Language();
                $e->setName($this->getText());
                $usuario->addLanguage($e);
            }

            for($k = 0 ; $k < $i ; $k++){
                $e = new OtherInfo();
                $e->setName($this->getText());
                $usuario->addOtherInfo($e);
            }

            for($k = 0 ; $k < $i ; $k++){
                $e = new Certification();
                $e->setName($this->getText());
                $e->setCenter($this->getText());
                $e->setDate(new \DateTime('now - '.rand(7000, 20000).' days'));
                $usuario->addCertification($e);
            }

            $manager->persist($usuario);
        }

        $manager->flush();
    }

    /**
     * Generador aleatorio de nombres de personas.
     * Aproximadamente genera un 50% de hombres y un 50% de mujeres.
     *
     * @return string Nombre aleatorio generado para el usuario.
     */
    private function getNombre()
    {
        // Los nombres más populares en España según el INE
        // Fuente: http://www.ine.es/daco/daco42/nombyapel/nombyapel.htm

        $hombres = array(
            'Antonio', 'José', 'Manuel', 'Francisco', 'Juan', 'David',
            'José Antonio', 'José Luis', 'Jesús', 'Javier', 'Francisco Javier',
            'Carlos', 'Daniel', 'Miguel', 'Rafael', 'Pedro', 'José Manuel',
            'Ángel', 'Alejandro', 'Miguel Ángel', 'José María', 'Fernando',
            'Luis', 'Sergio', 'Pablo', 'Jorge', 'Alberto'
        );
        $mujeres = array(
            'María Carmen', 'María', 'Carmen', 'Josefa', 'Isabel', 'Ana María',
            'María Dolores', 'María Pilar', 'María Teresa', 'Ana', 'Francisca',
            'Laura', 'Antonia', 'Dolores', 'María Angeles', 'Cristina', 'Marta',
            'María José', 'María Isabel', 'Pilar', 'María Luisa', 'Concepción',
            'Lucía', 'Mercedes', 'Manuela', 'Elena', 'Rosa María'
        );

        if (rand() % 2) {
            return $hombres[array_rand($hombres)];
        } else {
            return $mujeres[array_rand($mujeres)];
        }
    }

    /**
     * Generador aleatorio de apellidos de personas.
     *
     * @return string Apellido aleatorio generado para el usuario.
     */
    private function getApellidos()
    {
        // Los apellidos más populares en España según el INE
        // Fuente: http://www.ine.es/daco/daco42/nombyapel/nombyapel.htm

        $apellidos = array(
            'García', 'González', 'Rodríguez', 'Fernández', 'López', 'Martínez',
            'Sánchez', 'Pérez', 'Gómez', 'Martín', 'Jiménez', 'Ruiz',
            'Hernández', 'Díaz', 'Moreno', 'Álvarez', 'Muñoz', 'Romero',
            'Alonso', 'Gutiérrez', 'Navarro', 'Torres', 'Domínguez', 'Vázquez',
            'Ramos', 'Gil', 'Ramírez', 'Serrano', 'Blanco', 'Suárez', 'Molina',
            'Morales', 'Ortega', 'Delgado', 'Castro', 'Ortíz', 'Rubio', 'Marín',
            'Sanz', 'Iglesias', 'Nuñez', 'Medina', 'Garrido'
        );

        return $apellidos[array_rand($apellidos)].' '.$apellidos[array_rand($apellidos)];
    }

    private function getKnowledge()
    {

        $tec = array(
            'HTML', 'HTML5', 'MYSQL', 'SQL Server', 'Visual Basic', 'C++',
            'Symfony1.4', 'Zend Framework', 'PHP', 'Pascal', 'C', 'C#',
            'Symfony2', 'CSS', 'CSS3', 'Javascript', 'Mootools', 'jQuery',
            'Visual Studio', 'ASP.NET', 'ASP Classic', 'AJAX', 'Prototype', 'Ruby on Rails',
            'MongoDB'
        );

        $res = $tec[array_rand($tec)];

        $salida = new Knowledge();
        $salida->setName($res);
        return $salida;
    }

    private function getDireccion()
    {
        $prefijos = array('Calle', 'Avenida', 'Plaza');
        $nombres = array(
            'Lorem', 'Ipsum', 'Sitamet', 'Consectetur', 'Adipiscing',
            'Necsapien', 'Tincidunt', 'Facilisis', 'Nulla', 'Scelerisque',
            'Blandit', 'Ligula', 'Eget', 'Hendrerit', 'Malesuada', 'Enimsit'
        );

        return $prefijos[array_rand($prefijos)].' '.$nombres[array_rand($nombres)].', '.rand(1, 100)."\n"
               .$this->getCodigoPostal();
    }

    /**
     * Generador aleatorio de códigos postales
     *
     * @return string Código postal aleatorio generado para la tienda.
     */
    private function getCodigoPostal()
    {
        return sprintf('%02s%03s', rand(1, 52), rand(0, 999));
    }

    private function getText(){
        $nombres = array(
            'Lorem', 'Ipsum', 'Sitamet', 'Consectetur', 'Adipiscing',
            'Necsapien', 'Tincidunt', 'Facilisis', 'Nulla', 'Scelerisque',
            'Blandit', 'Ligula', 'Eget', 'Hendrerit', 'Malesuada', 'Enimsit'
        );
        return $nombres[array_rand($nombres)];
    }
}
