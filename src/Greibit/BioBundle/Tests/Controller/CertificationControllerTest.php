<?php

namespace Greibit\BioBundle\Tests\Controller;

use Symfony\Bundle\FrameworkBundle\Client;

class CertificationControllerTest extends AbstractWebTestCase
{
    /** @var Client $client */
    private $client = null;

    public function setUp()
    {
        $this->loadUserFixtures();

        $this->client = $this->getClientUsuario4();
    }

    public function testShowUserCertification(){
        $crawler = $this->client->request('GET', '/user/usuario4/certifications');
        $this->assertEquals(200, $this->client->getResponse()->getStatusCode());
        $this->assertEquals($crawler->filter('tr.certification-line')->count(), 4);
    }
}
