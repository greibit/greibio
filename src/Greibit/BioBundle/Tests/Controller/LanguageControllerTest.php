<?php

namespace Greibit\BioBundle\Tests\Controller;
use Symfony\Bundle\FrameworkBundle\Client;

class LanguageControllerTest extends AbstractWebTestCase
{
    /** @var Client $client */
    private $client = null;

    public function setUp()
    {
        $this->loadUserFixtures();

        $this->client = $this->getClientUsuario4();
    }

    public function testShowUserLanguage(){
        $crawler = $this->client->request('GET', '/user/usuario4/languages');
        $this->assertEquals(200, $this->client->getResponse()->getStatusCode());
        $this->assertEquals($crawler->filter('tr.language-line')->count(), 4);
    }
}
