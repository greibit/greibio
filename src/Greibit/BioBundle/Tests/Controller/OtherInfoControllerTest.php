<?php

namespace Greibit\BioBundle\Tests\Controller;
use Symfony\Bundle\FrameworkBundle\Client;

class OtherInfoControllerTest extends AbstractWebTestCase
{
    /** @var Client $client */
    private $client = null;

    public function setUp()
    {
        $this->loadUserFixtures();

        $this->client = $this->getClientUsuario4();
    }

    public function testShowUserOtherinfo(){
        $crawler = $this->client->request('GET', '/user/usuario4/other-infos');
        $this->assertEquals(200, $this->client->getResponse()->getStatusCode());
        $this->assertEquals($crawler->filter('tr.otherinfo-line')->count(), 4);
    }
}
