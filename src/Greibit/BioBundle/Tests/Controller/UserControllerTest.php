<?php

namespace Greibit\BioBundle\Tests\Controller;

use Liip\FunctionalTestBundle\Test\WebTestCase;
use Symfony\Bundle\FrameworkBundle\Client;

class UserControllerTest extends AbstractWebTestCase
{
    /*
    public function testCompleteScenario()
    {
        // Create a new client to browse the application
        $client = static::createClient();

        // Create a new entry in the database
        $crawler = $client->request('GET', '/user/');
        $this->assertTrue(200 === $client->getResponse()->getStatusCode());
        $crawler = $client->click($crawler->selectLink('Create a new entry')->link());

        // Fill in the form and submit it
        $form = $crawler->selectButton('Create')->form(array(
            'greibit_biobundle_usertype[field_name]'  => 'Test',
            // ... other fields to fill
        ));

        $client->submit($form);
        $crawler = $client->followRedirect();

        // Check data in the show view
        $this->assertTrue($crawler->filter('td:contains("Test")')->count() > 0);

        // Edit the document
        $crawler = $client->click($crawler->selectLink('Edit')->link());

        $form = $crawler->selectButton('Edit')->form(array(
            'greibit_biobundle_usertype[field_name]'  => 'Foo',
            // ... other fields to fill
        ));

        $client->submit($form);
        $crawler = $client->followRedirect();

        // Check the element contains an attribute with value equals "Foo"
        $this->assertTrue($crawler->filter('[value="Foo"]')->count() > 0);

        // Delete the document
        $client->submit($crawler->selectButton('Delete')->form());
        $crawler = $client->followRedirect();

        // Check the document has been delete on the list
        $this->assertNotRegExp('/Foo/', $client->getResponse()->getContent());
    }
    */

    /** @var Client $client */
    private $client = null;

    public function setUp()
    {
        $this->loadUserFixtures();

        $this->client = $this->getClientUsuario4();
    }

    public function testUserList(){
        $crawler = $this->client->request('GET', '/users');

        $this->assertEquals(200, $this->client->getResponse()->getStatusCode());
        $this->assertTrue($crawler->filter('h1:contains("User list")')->count() > 0);
    }

    public function testShowUser(){

        // Create a new entry in the database
        $crawler = $this->client->request('GET', '/user/usuario4/show');

        $this->assertEquals(200, $this->client->getResponse()->getStatusCode());
        $this->assertTrue($crawler->filter('nav a:contains("usuario4")')->count() > 0);
    }

    public function testPrivateEditUser(){
        $crawler = $this->client->request('GET', '/private/personal-info/edit');
        $this->assertEquals(200, $this->client->getResponse()->getStatusCode());

        $form = $crawler->selectButton('Edit')->form($this->editUserFormData());

        $this->client->submit($form);
        $this->client->followRedirect();

        //$nodes = $crawler->filter('input#user_email');
        //ldd($nodes->attr('value'));
        //$this->assertEquals(200, $this->client->getResponse()->getStatusCode());
        //$this->assertTrue($crawler->filter('[value="Usuariotest@greibio.com"]')->count() > 0);
    }
}
