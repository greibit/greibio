<?php

namespace Greibit\BioBundle\Tests\Controller;
use Symfony\Bundle\FrameworkBundle\Client;

class KnowledgeControllerTest extends AbstractWebTestCase
{
    /** @var Client $client */
    private $client = null;

    public function setUp()
    {
        $this->loadUserFixtures();

        $this->client = $this->getClientUsuario4();
    }

    public function testShowUserKnowledge(){
        $crawler = $this->client->request('GET', '/user/usuario4/knowledges');
        $this->assertEquals(200, $this->client->getResponse()->getStatusCode());
        $this->assertEquals($crawler->filter('tr.knowledge-line')->count(), 4);
    }
}
