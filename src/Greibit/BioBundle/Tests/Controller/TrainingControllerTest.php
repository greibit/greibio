<?php

namespace Greibit\BioBundle\Tests\Controller;

use Symfony\Bundle\FrameworkBundle\Client;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

class TrainingControllerTest extends AbstractWebTestCase
{
    /** @var Client $client */
    private $client = null;

    public function setUp()
    {
        $this->loadUserFixtures();

        $this->client = $this->getClientUsuario4();
    }

    public function testShowUserTraining(){
        $crawler = $this->client->request('GET', '/user/usuario4/trainings');
        $this->assertEquals(200, $this->client->getResponse()->getStatusCode());
        $this->assertEquals($crawler->filter('div.training-line')->count(), 4);
    }

    public function testCreateUserTraining(){
        $crawler = $this->client->request('POST', '/private/training/create', $this->TrainingFormData());
        $this->assertEquals(201, $this->client->getResponse()->getStatusCode());

        $crawler = $this->client->request('GET', '/user/usuario4/trainings');
        $this->assertEquals(200, $this->client->getResponse()->getStatusCode());
        $this->assertEquals($crawler->filter('div.training-line')->count(), 5);
    }
}
