<?php

namespace Greibit\BioBundle\Tests\Controller;

use Liip\FunctionalTestBundle\Test\WebTestCase;

class AbstractWebTestCase extends WebTestCase
{
    protected function getClientUsuario4(){
        return $this->getLoggedClient("usuario4", "usuario4");
    }

    protected function getLoggedClient($user, $pass){
        return $this->createClient(array(), array('PHP_AUTH_USER' => $user, 'PHP_AUTH_PW' => $pass));
    }

    protected function loadUserFixtures(){
        $classes = array(
            'Greibit\BioBundle\DataFixtures\MongoDB\Usuarios'
        );

        $this->loadFixtures($classes, null, 'doctrine_mongodb');
    }

    protected function requestWithAuth($method, $uri, $parameters = array())
    {
        $server = array('PHP_AUTH_USER' => 'usuario4', 'PHP_AUTH_PW' => 'usuario4');

        return $this->client->request($method, $uri, $parameters, array(), $server);
    }

    protected function editUserFormData(){
        return array(
            'user[name]'  => 'Usuario Test',
            'user[surname]'  => 'Usuario Test',
            'user[email]'  => 'Usuariotesat@greibio.com',
            'user[telephone]'  => '644555333',
            'user[address]'  => 'C/ test 4',
            'user[locality]'  => 'TestLocality',
            'user[city]'  => 'TestCity',
            'user[nacionality]'  => 'TestNationality',
            'user[birthday][date][month]'  => '12',
            'user[birthday][date][year]'  => '2012',
            'user[birthday][date][day]'  => '12',
            'user[birthday][time][hour]'  => '12',
            'user[birthday][time][minute]'  => '12',
        );
    }

    protected function TrainingFormData(){
        return array(
            'training[title]'  => 'TrainingTest',
            'training[academy]'  => 'TrainingTest',
            'training[startDate][month]'  => '12',
            'training[startDate][year]'  => '2012',
            'training[startDate][day]'  => '12',
            'training[finishDate][month]'  => '12',
            'training[finishDate][year]'  => '2012',
            'training[finishDate][day]'  => '12',
        );
    }
}
