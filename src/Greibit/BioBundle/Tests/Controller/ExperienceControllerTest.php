<?php

namespace Greibit\BioBundle\Tests\Controller;
use Symfony\Bundle\FrameworkBundle\Client;

class ExperienceControllerTest extends AbstractWebTestCase
{
    /** @var Client $client */
    private $client = null;

    public function setUp()
    {
        $this->loadUserFixtures();

        $this->client = $this->getClientUsuario4();
    }

    public function testShowUserExperience(){
        $crawler = $this->client->request('GET', '/user/usuario4/experiences');
        $this->assertEquals(200, $this->client->getResponse()->getStatusCode());
        $this->assertEquals($crawler->filter('tr.experience-line')->count(), 4);
    }
}
