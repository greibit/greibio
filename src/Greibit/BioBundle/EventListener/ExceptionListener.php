<?php

namespace Greibit\BioBundle\EventListener;

use FOS\RestBundle\View\View;
use FOS\RestBundle\View\ViewHandler;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Event\GetResponseForExceptionEvent;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\HttpExceptionInterface;

class ExceptionListener
{
    protected $request;
    protected $viewHandler;

    public function setRequest(Request $request = null)
    {
        $this->request = $request;
    }

    public function __construct(ViewHandler $viewHandler){
        $this->viewHandler = $viewHandler;
    }

    public function onKernelException(GetResponseForExceptionEvent $event)
    {
        if($this->request->isXmlHttpRequest()){

            $exception = $event->getException();

            $code = method_exists($exception, "getStatusCode") ?  $exception->getStatusCode() : 500;

            $info = array("message" => $exception->getMessage(), "status_code" => $code);

            $view = View::create($info, $code);
            $view->setFormat("json");

            $event->setResponse($this->viewHandler->handle($view));
        }
        /*
        // You get the exception object from the received event
        $exception = $event->getException();
        $message = sprintf(
            'My Error says: %s with code: %s',
            $exception->getMessage(),
            $exception->getCode()
        );

        // Customize your response object to display the exception details
        $response = new Response();
        $response->setContent($message);

        // HttpExceptionInterface is a special type of exception that
        // holds status code and header details
        if ($exception instanceof HttpExceptionInterface) {
            $response->setStatusCode($exception->getStatusCode());
            $response->headers->replace($exception->getHeaders());
        } else {
            $response->setStatusCode(500);
        }

        // Send the modified response object to the event
        $event->setResponse($response);
        */
    }
}