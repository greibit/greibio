<?php

namespace Greibit\BioBundle;

use Symfony\Component\HttpKernel\Bundle\Bundle;

class GreibitBioBundle extends Bundle
{
    public function getParent()
    {
        return 'FOSUserBundle';
    }
}
