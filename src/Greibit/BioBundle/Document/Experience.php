<?php

namespace Greibit\BioBundle\Document;

use Doctrine\ODM\MongoDB\Mapping\Annotations as MongoDB;
use Greibit\AdminBundle\Document\Company;

/** @MongoDB\EmbeddedDocument */
class Experience
{
    /** @MongoDB\Id(strategy="auto") */
    protected $id;

    /** @MongoDB\Date */
    protected $startDate;

    /** @MongoDB\Date */
    protected $finishDate;

    /** @MongoDB\ReferenceOne(targetDocument="Greibit\AdminBundle\Document\Company") */
    protected $company;

    /** @MongoDB\String */
    protected $work;

    /** @MongoDB\String */
    protected $project;


    /**
     * Get id
     *
     * @return id $id
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set startDate
     *
     * @param date $startDate
     * @return self
     */
    public function setStartDate($startDate)
    {
        $this->startDate = $startDate;
        return $this;
    }

    /**
     * Get startDate
     *
     * @return date $startDate
     */
    public function getStartDate()
    {
        return $this->startDate;
    }

    /**
     * Set finishDate
     *
     * @param date $finishDate
     * @return self
     */
    public function setFinishDate($finishDate)
    {
        $this->finishDate = $finishDate;
        return $this;
    }

    /**
     * Get finishDate
     *
     * @return date $finishDate
     */
    public function getFinishDate()
    {
        return $this->finishDate;
    }

    /**
     * Set company
     *
     * @param Greibit\AdminBundle\Document\Company $company
     * @return self
     */
    public function setCompany(\Greibit\AdminBundle\Document\Company $company)
    {
        $this->company = $company;
        return $this;
    }

    /**
     * Get company
     *
     * @return Greibit\AdminBundle\Document\Company $company
     */
    public function getCompany()
    {
        return $this->company;
    }

    /**
     * Set work
     *
     * @param string $work
     * @return self
     */
    public function setWork($work)
    {
        $this->work = $work;
        return $this;
    }

    /**
     * Get work
     *
     * @return string $work
     */
    public function getWork()
    {
        return $this->work;
    }

    /**
     * Set project
     *
     * @param string $project
     * @return self
     */
    public function setProject($project)
    {
        $this->project = $project;
        return $this;
    }

    /**
     * Get project
     *
     * @return string $project
     */
    public function getProject()
    {
        return $this->project;
    }
}
