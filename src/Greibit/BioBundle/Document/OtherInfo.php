<?php
/**
 * Created by JetBrains PhpStorm.
 * User: Greibit PC
 * Date: 28/07/13
 * Time: 11:42
 * To change this template use File | Settings | File Templates.
 */

namespace Greibit\BioBundle\Document;
use Doctrine\ODM\MongoDB\Mapping\Annotations as MongoDB;

/** @MongoDB\EmbeddedDocument */
class OtherInfo
{
    /** @MongoDB\Id(strategy="auto") */
    protected $id;

    /** @MongoDB\String */
    protected $name;

    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     * @return self
     */
    public function setName($name)
    {
        $this->name = $name;
        return $this;
    }

    /**
     * Get name
     *
     * @return string $name
     */
    public function getName()
    {
        return $this->name;
    }
}
