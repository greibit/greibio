<?php
namespace Greibit\BioBundle\Document;

use FOS\UserBundle\Model\User as BaseUser;
use Doctrine\ODM\MongoDB\Mapping\Annotations as MongoDB;

/**
 * @MongoDB\Document
 */
class User extends BaseUser
{
    /** @MongoDB\Id(strategy="auto") */
    protected $id;

    /** @MongoDB\String */
    private $name;

    /** @MongoDB\String */
    private $surname;

    /** @MongoDB\String */
    private $address;

    /** @MongoDB\Date */
    private $birthday;

    /** @MongoDB\String */
    private $locality;

    /** @MongoDB\String */
    private $city;

    /** @MongoDB\String */
    private $nacionality;

    /** @MongoDB\String */
    private $telephone;

    /** @MongoDB\EmbedMany(targetDocument="Experience") */
    private $experiences = array();

    /** @MongoDB\EmbedMany(targetDocument="Training") */
    private $trainings = array();

    /** @MongoDB\EmbedMany(targetDocument="Language") */
    private $languages = array();

    /** @MongoDB\EmbedMany(targetDocument="Knowledge") */
    private $knowledges = array();

    /** @MongoDB\EmbedMany(targetDocument="OtherInfo") */
    private $otherInfos = array();

    /** @MongoDB\EmbedMany(targetDocument="Certification") */
    private $certifications = array();

    public function __construct()
    {
        parent::__construct();
        // your own logic
    }

    /**
     * Get id
     *
     * @return integer $id
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Add experiences
     *
     * @param \Greibit\BioBundle\Document\Experience $experiences
     */
    public function addExperience(Experience $experiences)
    {
        $this->experiences[] = $experiences;
    }

    /**
     * Remove experiences
     *
     * @param \Greibit\BioBundle\Document\Experience $experiences
     */
    public function removeExperience(Experience $experiences)
    {
        $this->experiences->removeElement($experiences);
    }

    /**
     * Get experiences
     *
     * @return \Doctrine\Common\Collections\Collection $experiences
     */
    public function getExperiences()
    {
        $salida = $this->experiences->toArray();

        usort($salida, function($a1, $a2) {
            $v1 = $a1->getStartDate()->getTimestamp();
            $v2 = $a2->getStartDate()->getTimestamp();
            return $v1 - $v2; // $v2 - $v1 to reverse direction
        });

        return $salida;
    }

    /**
     * @param $id
     * @return null|Experience
     */
    public function getSingleExperience($id)
    {
        foreach($this->experiences as $experience){
            if($experience->getId() === $id){
                return $experience;
            }
        }
        return null;
    }

    /**
     * Add languages
     *
     * @param \Greibit\BioBundle\Document\Language $languages
     */
    public function addLanguage(Language $languages)
    {
        $this->languages[] = $languages;
    }

    /**
     * Remove languages
     *
     * @param \Greibit\BioBundle\Document\Language $languages
     */
    public function removeLanguage(Language $languages)
    {
        $this->languages->removeElement($languages);
    }

    /**
     * Get languages
     *
     * @return \Doctrine\Common\Collections\Collection $languages
     */
    public function getLanguages()
    {
        return $this->languages;
    }

    /**
     * @param $id
     * @return null|Language
     */
    public function getSingleLanguage($id)
    {
        foreach($this->languages as $language){
            if($language->getId() === $id){
                return $language;
            }
        }
        return null;
    }

    /**
     * Add knowledges
     *
     * @param \Greibit\BioBundle\Document\Knowledge $knowledges
     */
    public function addKnowledge(Knowledge $knowledges)
    {
        $this->knowledges[] = $knowledges;
    }

    /**
     * Remove knowledges
     *
     * @param \Greibit\BioBundle\Document\Knowledge $knowledges
     */
    public function removeKnowledge(Knowledge $knowledges)
    {
        $this->knowledges->removeElement($knowledges);
    }

    /**
     * Get knowledges
     *
     * @return \Doctrine\Common\Collections\Collection $knowledges
     */
    public function getKnowledges()
    {
        return $this->knowledges;
    }

    /**
     * @param $id
     * @return null|Knowledge
     */
    public function getSingleKnowledge($id)
    {
        foreach($this->knowledges as $knowledge){
            if($knowledge->getId() === $id){
                return $knowledge;
            }
        }
        return null;
    }
    /**
     * Add otherInfos
     *
     * @param \Greibit\BioBundle\Document\OtherInfo $otherInfos
     */
    public function addOtherInfo(OtherInfo $otherInfos)
    {
        $this->otherInfos[] = $otherInfos;
    }

    /**
     * Remove otherInfos
     *
     * @param \Greibit\BioBundle\Document\OtherInfo $otherInfos
     */
    public function removeOtherInfo(OtherInfo $otherInfos)
    {
        $this->otherInfos->removeElement($otherInfos);
    }

    /**
     * Get otherInfos
     *
     * @return \Doctrine\Common\Collections\Collection $otherInfos
     */
    public function getOtherInfos()
    {
        return $this->otherInfos;
    }

    /**
     * @param $id
     * @return null|OtherInfo
     */
    public function getSingleOtherInfo($id)
    {
        foreach($this->otherInfos as $otherInfo){
            if($otherInfo->getId() === $id){
                return $otherInfo;
            }
        }
        return null;
    }

    /**
     * Add certifications
     *
     * @param \Greibit\BioBundle\Document\Certification $certifications
     */
    public function addCertification(Certification $certifications)
    {
        $this->certifications[] = $certifications;
    }

    /**
     * Remove certifications
     *
     * @param \Greibit\BioBundle\Document\Certification $certifications
     */
    public function removeCertification(Certification $certifications)
    {
        $this->certifications->removeElement($certifications);
    }

    /**
     * Get certifications
     *
     * @return \Doctrine\Common\Collections\Collection $certifications
     */
    public function getCertifications()
    {
        return $this->certifications;
    }

    /**
     * @param $id
     * @return null|Certification
     */
    public function getSingleCertification($id)
    {
        foreach($this->certifications as $certification){
            if($certification->getId() === $id){
                return $certification;
            }
        }
        return null;
    }

    /**
     * Add trainings
     *
     * @param \Greibit\BioBundle\Document\Training $trainings
     */
    public function addTraining(Training $trainings)
    {
        $this->trainings[] = $trainings;
    }

    /**
     * Remove trainings
     *
     * @param \Greibit\BioBundle\Document\Training $trainings
     */
    public function removeTraining(Training $trainings)
    {
        $this->trainings->removeElement($trainings);
    }

    /**
     * Get trainings
     *
     * @return \Doctrine\Common\Collections\Collection $trainings
     */
    public function getTrainings()
    {
        $salida = $this->trainings->toArray();

        usort($salida, function($a1, $a2) {
            $v1 = $a1->getStartDate()->getTimestamp();
            $v2 = $a2->getStartDate()->getTimestamp();
            return $v1 - $v2; // $v2 - $v1 to reverse direction
        });

        return $salida;
    }

    /**
     * @param $id
     * @return null|Training
     */
    public function getSingleTraining($id)
    {
        foreach($this->trainings as $training){
            if($training->getId() === $id){
                return $training;
            }
        }
        return null;
    }

    /**
     * Set name
     *
     * @param string $name
     * @return self
     */
    public function setName($name)
    {
        $this->name = $name;
        return $this;
    }

    /**
     * Get name
     *
     * @return string $name
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set surname
     *
     * @param string $surname
     * @return self
     */
    public function setSurname($surname)
    {
        $this->surname = $surname;
        return $this;
    }

    /**
     * Get surname
     *
     * @return string $surname
     */
    public function getSurname()
    {
        return $this->surname;
    }

    /**
     * Set address
     *
     * @param string $address
     * @return self
     */
    public function setAddress($address)
    {
        $this->address = $address;
        return $this;
    }

    /**
     * Get address
     *
     * @return string $address
     */
    public function getAddress()
    {
        return $this->address;
    }

    /**
     * Set birthday
     *
     * @param \DateTime $birthday
     * @return self
     */
    public function setBirthday($birthday)
    {
        $this->birthday = $birthday;
        return $this;
    }

    /**
     * Get birthday
     *
     * @return \DateTime $birthday
     */
    public function getBirthday()
    {
        return $this->birthday;
    }

    /**
     * Set locality
     *
     * @param string $locality
     * @return self
     */
    public function setLocality($locality)
    {
        $this->locality = $locality;
        return $this;
    }

    /**
     * Get locality
     *
     * @return string $locality
     */
    public function getLocality()
    {
        return $this->locality;
    }

    /**
     * Set city
     *
     * @param string $city
     * @return self
     */
    public function setCity($city)
    {
        $this->city = $city;
        return $this;
    }

    /**
     * Get city
     *
     * @return string $city
     */
    public function getCity()
    {
        return $this->city;
    }

    /**
     * Set telephone
     *
     * @param string $telephone
     * @return self
     */
    public function setTelephone($telephone)
    {
        $this->telephone = $telephone;
        return $this;
    }

    /**
     * Get telephone
     *
     * @return string $telephone
     */
    public function getTelephone()
    {
        return $this->telephone;
    }

    /**
     * Set nacionality
     *
     * @param string $nacionality
     * @return self
     */
    public function setNacionality($nacionality)
    {
        $this->nacionality = $nacionality;
        return $this;
    }

    /**
     * Get nacionality
     *
     * @return string $nacionality
     */
    public function getNacionality()
    {
        return $this->nacionality;
    }

    public function setSalt($salt)
    {
        $this->salt = $salt;
        return $this;
    }
}
