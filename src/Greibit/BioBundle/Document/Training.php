<?php

namespace Greibit\BioBundle\Document;
use Doctrine\ODM\MongoDB\Mapping\Annotations as MongoDB;

/** @MongoDB\EmbeddedDocument */
class Training
{
    /** @MongoDB\Id(strategy="auto") */
    protected $id;

    /** @MongoDB\Date */
    protected $startDate;

    /** @MongoDB\Date */
    protected $finishDate;

    /** @MongoDB\String */
    protected $title;

    /** @MongoDB\ReferenceOne(targetDocument="Greibit\AdminBundle\Document\Academy") */
    protected $academy;

    public function getId()
    {
        return $this->id;
    }

    /**
     * Set startDate
     *
     * @param \Datetime $startDate
     * @return self
     */
    public function setStartDate($startDate)
    {
        $this->startDate = $startDate;
        return $this;
    }

    /**
     * Get startDate
     *
     * @return \Datetime $startDate
     */
    public function getStartDate()
    {
        return $this->startDate;
    }

    /**
     * Set finishDate
     *
     * @param \Datetime $finishDate
     * @return self
     */
    public function setFinishDate($finishDate)
    {
        $this->finishDate = $finishDate;
        return $this;
    }

    /**
     * Get finishDate
     *
     * @return \Datetime $finishDate
     */
    public function getFinishDate()
    {
        return $this->finishDate;
    }

    /**
     * Set title
     *
     * @param string $title
     * @return self
     */
    public function setTitle($title)
    {
        $this->title = $title;
        return $this;
    }

    /**
     * Get title
     *
     * @return string $title
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * Set academy
     *
     * @param \Greibit\AdminBundle\Document\Academy $academy
     * @return self
     */
    public function setAcademy(\Greibit\AdminBundle\Document\Academy $academy)
    {
        $this->academy = $academy;
        return $this;
    }

    /**
     * Get academy
     *
     * @return \Greibit\AdminBundle\Document\Academy $academy
     */
    public function getAcademy()
    {
        return $this->academy;
    }
}
