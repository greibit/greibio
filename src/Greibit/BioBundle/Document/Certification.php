<?php

namespace Greibit\BioBundle\Document;
use Doctrine\ODM\MongoDB\Mapping\Annotations as MongoDB;
use Greibit\AdminBundle\Document\Academy;

/** @MongoDB\EmbeddedDocument */
class Certification
{
    /** @MongoDB\Id(strategy="auto") */
    protected $id;

    /** @MongoDB\String */
    protected $name;

    /** @MongoDB\Date */
    protected $date;

    /** @MongoDB\ReferenceOne(targetDocument="Greibit\AdminBundle\Document\Academy") */
    protected $academy;

    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     * @return self
     */
    public function setName($name)
    {
        $this->name = $name;
        return $this;
    }

    /**
     * Get name
     *
     * @return string $name
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set date
     *
     * @param string $date
     * @return self
     */
    public function setDate($date)
    {
        $this->date = $date;
        return $this;
    }

    /**
     * Get date
     *
     * @return string $date
     */
    public function getDate()
    {
        return $this->date;
    }

    /**
     * Set academy
     *
     * @param Academy $academy
     * @return self
     */
    public function setAcademy(Academy $academy)
    {
        $this->academy = $academy;
        return $this;
    }

    /**
     * Get academy
     *
     * @return Academy $academy
     */
    public function getAcademy()
    {
        return $this->academy;
    }
}
