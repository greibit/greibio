<?php
/**
 * Created by JetBrains PhpStorm.
 * User: Greibit PC
 * Date: 4/10/13
 * Time: 18:21
 * To change this template use File | Settings | File Templates.
 */

namespace Greibit\BioBundle\Controller;

use Doctrine\ODM\MongoDB\DocumentManager;
use FOS\RestBundle\Controller\FOSRestController;
use Greibit\BioBundle\Document\User;
use Symfony\Component\HttpFoundation\Response;

class AbstractController extends FOSRestController
{
    protected function makeCreate(User $user, $class){
        $request = $this->getRequest();
        $documentClass = $this->getDocumentClass($class);
        $formClass = $this->getFormClass($class);

        $document = new $documentClass();
        $form     = $this->createForm(new $formClass(), $document);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $dm = $this->getDocumentManager();
            call_user_func(array($user, "add".$class), $document);
            $dm->persist($user);
            $dm->flush();

            return $this->jsonSuccessResponse($class." was created", 201);
        }

        return $this->jsonFailResponse($form->getErrors(), 400);
    }

    protected function makeUpdate(User $user, $itemId, $class){
        $dm = $this->getDocumentManager();

        $document = $this->getDocument($user, $itemId, $class);

        $request = $this->getRequest();
        $formClass = $this->getFormClass($class);

        $form     = $this->createForm(new $formClass(), $document);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $dm->persist($document);
            $dm->flush();

            return $this->jsonSuccessResponse($class." was created", 201);
        }

        return $this->jsonFailResponse($form->getErrors(), 400);
    }

    protected function makeDelete(User $user, $itemId, $class){
        $dm = $this->getDocumentManager();

        $document = $this->getDocument($user, $itemId, $class);
        call_user_func(array($user, "remove".$class), $document);

        $dm->persist($user);
        $dm->flush();
        return $this->jsonSuccessResponse($class." borrado correctamente", 200);
    }

    protected function displayEditForm($id, User $user, $class){
        $document = $this->getDocument($user, $id, $class);

        $formClass = $this->getFormClass($class);

        $editForm = $this->createForm(new $formClass(), $document);

        return array(
            'document'    => $document,
            'user' => $user,
            'edit_form'   => $editForm->createView(),
        );
    }

    protected function displayNewForm(User $user, $class){
        $documentClass = $this->getDocumentClass($class);
        $formClass = $this->getFormClass($class);

        $document = new $documentClass();
        $form     = $this->createForm(new $formClass(), $document);

        return array(
            'document'    => $document,
            'user' => $user,
            'form'   => $form->createView(),
        );
    }

    protected function getDocument($user, $id, $class){
        $document = call_user_func(array($user, "getSingle".$class), $id);
        if(!$document){
            throw $this->createNotFoundException('Unable to find '.$class.' document.');
        }
        return $document;
    }
    protected function getDocumentClass($class){
        return "Greibit\\BioBundle\\Document\\".$class;
    }
    protected function getFormClass($class){
        return "Greibit\\BioBundle\\Form\\".$class."Type";
    }

    /**
     * @param $errors
     * @param $statusCode
     * @return Response
     */
    protected function jsonFailResponse($errors, $statusCode){
        $info = array("errors" => $errors, "status_code" => $statusCode);

        return $this->handleJsonView($info, $statusCode);
    }

    /**
     * @param $message
     * @param $statusCode
     * @return Response
     */
    protected function jsonSuccessResponse($message, $statusCode){
        $info = array("message" => $message, "status_code" => $statusCode);

        return $this->handleJsonView($info, $statusCode);
    }

    /**
     * @param $data
     * @param $statusCode
     * @return Response
     */
    protected function handleJsonView($data, $statusCode){
        $view = $this->view($data, $statusCode);
        $view->setFormat("json");
        return $this->handleView($view);
    }

    /**
     * Returns the DocumentManager
     *
     * @return DocumentManager
     */
    protected function getDocumentManager()
    {
        return $this->get('doctrine.odm.mongodb.document_manager');
    }
}