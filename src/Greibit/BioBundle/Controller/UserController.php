<?php

namespace Greibit\BioBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Greibit\BioBundle\Document\User;
use Greibit\BioBundle\Form\UserType;
use Symfony\Component\HttpFoundation\Response;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;

class UserController extends Controller
{
    /**
     * Lists all User documents.
     *
     * @Route("/users", name="users")
     * @Template()
     *
     * @return array
     */
    public function indexAction()
    {
        $dm = $this->getDocumentManager();

        $documents = $dm->getRepository('GreibitBioBundle:User')->findAll();

        return array('documents' => $documents);
    }

    /**
     * Displays a form to create a new User document.
     *
     * @Route("/admin/user/new", name="admin_user_new")
     * @Template()
     *
     * @return array
     */
    public function newAction()
    {
        $document = new User();
        $form = $this->createForm(new UserType(), $document);

        return array(
            'document' => $document,
            'form'     => $form->createView()
        );
    }

    /**
     * Creates a new User document.
     *
     * @Route("/admin/user/create", name="admin_user_create")
     * @Method("POST")
     * @Template("GreibitBioBundle:User:new.html.twig")
     *
     * @param Request $request
     *
     * @return array
     */
    public function createAction(Request $request)
    {
        $document = new User();
        $form     = $this->createForm(new UserType(), $document);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $dm = $this->getDocumentManager();
            $dm->persist($document);
            $dm->flush();

            return $this->redirect($this->generateUrl('user_show', array('username' => $document->getUsername())));
        }

        return array(
            'document' => $document,
            'form'     => $form->createView()
        );
    }

    /**
     * Finds and displays a User document.
     * @Route("/user/{username}/show", name="public_user_show")
     * @ParamConverter("user", class="Greibit\BioBundle\Document\User", options={"username" = "username"})
     * @Template()
     */
    public function showAction(User $user)
    {
        return array(
            'user' => $user,
        );
    }

    /**
     * Displays a form to edit an existing User document.
     *
     * @Route("/private/personal-info/edit", name="private_user_edit")
     * @Template()
     *
     * @return array
     *
     * @throws \Symfony\Component\HttpKernel\Exception\NotFoundHttpException If document doesn't exists
     */
    public function editAction()
    {
        $user = $this->getUser();
        if (!$user) {
            throw $this->createNotFoundException('Unable to find User document.');
        }

        $editForm = $this->createForm(new UserType(), $user);

        return array(
            'user'    => $user,
            'edit_form'   => $editForm->createView(),
        );
    }

    /**
     * Edits an existing User document.
     *
     * @Route("/private/personal-info/update", name="private_user_update")
     * @Method("POST")
     * @Template("GreibitBioBundle:User:edit.html.twig")
     *
     * @param Request $request The request object
     *
     * @return array
     *
     * @throws \Symfony\Component\HttpKernel\Exception\NotFoundHttpException If document doesn't exists
     */
    public function updateAction(Request $request)
    {
        $dm = $this->getDocumentManager();

        $user = $this->getUser();
        if (!$user) {
            throw $this->createNotFoundException('Unable to find User document.');
        }

        $editForm   = $this->createForm(new UserType(), $user);
        $editForm->handleRequest($request);

        if ($editForm->isValid()) {

            $dm->persist($user);
            $dm->flush();

            return $this->redirect($this->generateUrl('private_user_edit'));
        }

        return array(
            'user'    => $user,
            'edit_form'   => $editForm->createView(),
        );
    }

    /**
     * Deletes a User document.
     *
     * @Route("/private/profile/delete", name="private_user_delete")
     * @Method("POST")
     *
     * @param Request $request The request object
     * @param string $id       The document ID
     *
     * @return array
     *
     * @throws \Symfony\Component\HttpKernel\Exception\NotFoundHttpException If document doesn't exists
     */
    public function deleteAction(Request $request, $id)
    {
        $form = $this->createDeleteForm($id);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $dm = $this->getDocumentManager();
            $document = $dm->getRepository('GreibitBioBundle:User')->find($id);

            if (!$document) {
                throw $this->createNotFoundException('Unable to find User document.');
            }

            $dm->remove($document);
            $dm->flush();
        }

        return $this->redirect($this->generateUrl('user'));
    }

    private function createDeleteForm($id)
    {
        return $this->createFormBuilder(array('id' => $id))
            ->add('id', 'hidden')
            ->getForm()
        ;
    }

    /**
     * Returns the DocumentManager
     *
     * @return DocumentManager
     */
    private function getDocumentManager()
    {
        return $this->get('doctrine.odm.mongodb.document_manager');
    }
}
