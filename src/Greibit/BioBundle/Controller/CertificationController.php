<?php

namespace Greibit\BioBundle\Controller;

use Greibit\BioBundle\Document\User;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;

class CertificationController extends AbstractController
{
    const DOCUMENT_NAME = "Certification";
    /**
     * @Route("/user/{username}/certifications", name="get_user_certifications", options={"expose" = true})
     * @Template("GreibitBioBundle:Certification:data.html.twig")
     * @ParamConverter("user", class="Greibit\BioBundle\Document\User", options={"username" = "username"})
     */
    public function indexAction(User $user)
    {
        return array(
            "user" => $user,
            strtolower(self::DOCUMENT_NAME."s") => $user->getCertifications(),
        );
    }
    
    /**
     * Displays a form to create a new Certification document.
     *
     * @Route("/private/certification/new", name="private_user_certification_new")
     * @Template()
     */
    public function newAction()
    {
        return $this->displayNewForm($this->getUser(), self::DOCUMENT_NAME);
    }

    /**
     * Creates a new Certification document.
     *
     * @Route("/private/certification/create", name="private_user_certification_create")
     * @Method("POST")
     * @Template("GreibitBioBundle:Certification:new.html.twig")
     */
    public function createAction()
    {
        return $this->makeCreate($this->getUser(), self::DOCUMENT_NAME);
    }

    /**
     * Displays a form to edit an existing Certification document.
     *
     * @Route("/private/certification/{id}/edit", name="private_user_certification_edit")
     * @Template()
     */
    public function editAction($id)
    {
        return $this->displayEditForm($id, $this->getUser(), self::DOCUMENT_NAME);
    }

    /**
     * Edits an existing Certification document.
     *
     * @Route("/private/certification/{id}/update", name="private_user_certification_update")
     * @Method("POST")
     */
    public function updateAction($id)
    {
        return $this->makeUpdate($this->getUser(), $id, self::DOCUMENT_NAME);
    }

    /**
     * Deletes a Certification document.
     *
     * @Route("/private/certification/{id}/delete", name="private_user_certification_delete")
     * @Method("POST")
     */
    public function deleteAction($id)
    {
        return $this->makeDelete($this->getUser(), $id, self::DOCUMENT_NAME);
    }
}
