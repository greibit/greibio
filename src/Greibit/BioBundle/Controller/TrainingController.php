<?php

namespace Greibit\BioBundle\Controller;

use Greibit\BioBundle\Document\User;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;


class TrainingController extends AbstractController
{
    const DOCUMENT_NAME = "Training";
    /**
     * @Route("/user/{username}/trainings", name="get_user_trainings", options={"expose" = true})
     * @Template("GreibitBioBundle:Training:data.html.twig")
     * @ParamConverter("user", class="Greibit\BioBundle\Document\User", options={"username" = "username"})
     */
    public function indexAction(User $user)
    {
        return array(
            "user" => $user,
            strtolower(self::DOCUMENT_NAME."s") => $user->getTrainings(),
        );
    }

    /**
     * Displays a form to create a new Training document.
     *
     * @Route("/private/training/new", name="private_user_training_new")
     * @Template()
     *
     */
    public function newAction()
    {
        return $this->displayNewForm($this->getUser(), self::DOCUMENT_NAME);
    }

    /**
     * Creates a new Training document.
     *
     * @Route("/private/training/create", name="private_user_training_create")
     * @Method("POST")
     * @Template("GreibitBioBundle:Training:new.html.twig")
     */
    public function createAction()
    {
        return $this->makeCreate($this->getUser(), self::DOCUMENT_NAME);
    }

    /**
     * Displays a form to edit an existing Training document.
     *
     * @Route("/private/training/{id}/edit", name="private_user_training_edit")
     * @Template()
     *
     */
    public function editAction($id)
    {
        return $this->displayEditForm($id, $this->getUser(), self::DOCUMENT_NAME);
    }

    /**
     * Edits an existing Training document.
     *
     * @Route("/private/training/{id}/update", name="private_user_training_update")
     * @Method("POST")
     */
    public function updateAction($id)
    {
        return $this->makeUpdate($this->getUser(), $id, self::DOCUMENT_NAME);
    }

    /**
     * Deletes a Training document.
     *
     * @Route("/private/training/{id}/delete", name="private_user_training_delete")
     * @Method("POST")
     *
     */
    public function deleteAction($id)
    {
        return $this->makeDelete($this->getUser(), $id, self::DOCUMENT_NAME);
    }
}
