<?php

namespace Greibit\BioBundle\Controller;

use Greibit\BioBundle\Document\User;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;

class ExperienceController extends AbstractController
{
    const DOCUMENT_NAME = "Experience";
    /**
     * @Route("/user/{username}/experiences", name="get_user_experiences", options={"expose" = true})
     * @Template("GreibitBioBundle:Experience:data.html.twig")
     * @ParamConverter("user", class="Greibit\BioBundle\Document\User", options={"username" = "username"})
     */
    public function indexAction(User $user)
    {
        return array(
            "user" => $user,
            strtolower(self::DOCUMENT_NAME."s") => $user->getExperiences(),
        );
    }
    
    /**
     * Displays a form to create a new Experience document.
     *
     * @Route("/private/experience/new", name="private_user_experience_new")
     * @Template()
     */
    public function newAction()
    {
        $user = $this->getUser();
        $documentClass = $this->getDocumentClass(self::DOCUMENT_NAME);
        $formClass = $this->getFormClass(self::DOCUMENT_NAME);

        $document = new $documentClass();
        $form     = $this->createForm(new $formClass(), $document, array(
            'em' => $this->getDocumentManager(),
        ));

        return array(
            'document'    => $document,
            'user' => $user,
            'form'   => $form->createView(),
        );
    }

    /**
     * Creates a new Experience document.
     *
     * @Route("/private/experience/create", name="private_user_experience_create")
     * @Method("POST")
     * @Template("GreibitBioBundle:Experience:new.html.twig")
     *
     */
    public function createAction()
    {
        $user = $this->getUser();
        $request = $this->getRequest();
        $documentClass = $this->getDocumentClass(self::DOCUMENT_NAME);
        $formClass = $this->getFormClass(self::DOCUMENT_NAME);

        $document = new $documentClass();
        $form     = $this->createForm(new $formClass(), $document, array(
            'em' => $this->getDocumentManager(),
        ));
        $form->handleRequest($request);

        if ($form->isValid()) {
            $dm = $this->getDocumentManager();
            call_user_func(array($user, "add".self::DOCUMENT_NAME), $document);
            $dm->persist($user);
            $dm->flush();

            return $this->jsonSuccessResponse(self::DOCUMENT_NAME." was created", 201);
        }

        return $this->jsonFailResponse($form->getErrors(), 400);
    }

    /**
     * Displays a form to edit an existing Experience document.
     *
     * @Route("/private/experience/{id}/edit", name="private_user_experience_edit")
     * @Template()
     *
     */
    public function editAction($id)
    {
        return $this->displayEditForm($id, $this->getUser(), self::DOCUMENT_NAME);
    }

    /**
     * Edits an existing Experience document.
     *
     * @Route("/private/experience/{id}/update", name="private_user_experience_update")
     * @Method("POST")
     *
     */
    public function updateAction($id)
    {
        return $this->makeUpdate($this->getUser(), $id, self::DOCUMENT_NAME);
    }

    /**
     * Deletes a Experience document.
     *
     * @Route("/private/experience/{id}/delete", name="private_user_experience_delete")
     * @Method("POST")
     *
     */
    public function deleteAction($id)
    {
        return $this->makeDelete($this->getUser(), $id, self::DOCUMENT_NAME);
    }
}
