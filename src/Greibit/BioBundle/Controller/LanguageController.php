<?php

namespace Greibit\BioBundle\Controller;

use Greibit\BioBundle\Document\User;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;

class LanguageController extends AbstractController
{
    const DOCUMENT_NAME = "Language";
    /**
     * @Route("/user/{username}/languages", name="get_user_languages", options={"expose" = true})
     * @Template("GreibitBioBundle:Language:data.html.twig")
     * @ParamConverter("user", class="Greibit\BioBundle\Document\User", options={"username" = "username"})
     */
    public function indexAction(User $user)
    {
        return array(
            "user" => $user,
            strtolower(self::DOCUMENT_NAME."s") => $user->getLanguages(),
        );
    }
    
    /**
     * Displays a form to create a new Language document.
     *
     * @Route("/private/language/new", name="private_user_language_new")
     * @Template()
     */
    public function newAction()
    {
        return $this->displayNewForm($this->getUser(), self::DOCUMENT_NAME);
    }

    /**
     * Creates a new Language document.
     *
     * @Route("/private/language/create", name="private_user_language_create")
     * @Method("POST")
     * @Template("GreibitBioBundle:Language:new.html.twig")
     *
     */
    public function createAction()
    {
        return $this->makeCreate($this->getUser(), self::DOCUMENT_NAME);
    }

    /**
     * Displays a form to edit an existing Language document.
     *
     * @Route("/private/language/{id}/edit", name="private_user_language_edit")
     * @Template()
     *
     */
    public function editAction($id)
    {
        return $this->displayEditForm($id, $this->getUser(), self::DOCUMENT_NAME);
    }

    /**
     * Edits an existing Language document.
     *
     * @Route("/private/language/{id}/update", name="private_user_language_update")
     * @Method("POST")
     * @Template("GreibitBioBundle:Language:edit.html.twig")
     *
     */
    public function updateAction($id)
    {
        return $this->makeUpdate($this->getUser(), $id, self::DOCUMENT_NAME);
    }

    /**
     * Deletes a Language document.
     *
     * @Route("/private/language/{id}/delete", name="private_user_language_delete")
     * @Method("POST")
     *
     */
    public function deleteAction($id)
    {
        return $this->makeDelete($this->getUser(), $id, self::DOCUMENT_NAME);
    }
}
