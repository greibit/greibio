<?php

namespace Greibit\BioBundle\Controller;

use Greibit\BioBundle\Document\User;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;

class OtherInfoController extends AbstractController
{
    const DOCUMENT_NAME = "OtherInfo";
    /**
     * @Route("/user/{username}/other-infos", name="get_user_otherinfos", options={"expose" = true})
     * @Template("GreibitBioBundle:OtherInfo:data.html.twig")
     * @ParamConverter("user", class="Greibit\BioBundle\Document\User", options={"username" = "username"})
     */
    public function indexAction(User $user)
    {
        return array(
            "user" => $user,
            strtolower(self::DOCUMENT_NAME."s") => $user->getOtherInfos(),
        );
    }
    /**
     * Displays a form to create a new OtherInfo document.
     *
     * @Route("/private/other-info/new", name="private_user_otherinfo_new")
     * @Template()
     */
    public function newAction()
    {
        return $this->displayNewForm($this->getUser(), self::DOCUMENT_NAME);
    }

    /**
     * Creates a new OtherInfo document.
     *
     * @Route("/private/other-info/create", name="private_user_otherinfo_create")
     * @Method("POST")
     * @Template("GreibitBioBundle:OtherInfo:new.html.twig")
     *
     */
    public function createAction()
    {
        return $this->makeCreate($this->getUser(), self::DOCUMENT_NAME);
    }

    /**
     * Displays a form to edit an existing OtherInfo document.
     *
     * @Route("/private/other-info/{id}/edit", name="private_user_otherinfo_edit")
     * @Template()
     *
     */
    public function editAction($id)
    {
        return $this->displayEditForm($id, $this->getUser(), self::DOCUMENT_NAME);
    }

    /**
     * Edits an existing OtherInfo document.
     *
     * @Route("/private/other-info/{id}/update", name="private_user_otherinfo_update")
     * @Method("POST")
     * @Template("GreibitBioBundle:OtherInfo:edit.html.twig")
     *
     */
    public function updateAction($id)
    {
        return $this->makeUpdate($this->getUser(), $id, self::DOCUMENT_NAME);
    }

    /**
     * Deletes a OtherInfo document.
     *
     * @Route("/private/other-info/{id}/delete", name="private_user_otherinfo_delete")
     * @Method("POST")
     *
     */
    public function deleteAction($id)
    {
        return $this->makeDelete($this->getUser(), $id, self::DOCUMENT_NAME);
    }
}
