<?php

namespace Greibit\BioBundle\Controller;

use Greibit\BioBundle\Document\User;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;

class KnowledgeController extends AbstractController
{
    const DOCUMENT_NAME = "Knowledge";
    /**
     * @Route("/user/{username}/knowledges", name="get_user_knowledges", options={"expose" = true})
     * @Template("GreibitBioBundle:Knowledge:data.html.twig")
     * @ParamConverter("user", class="Greibit\BioBundle\Document\User", options={"username" = "username"})
     */
    public function indexAction(User $user)
    {
        return array(
            "user" => $user,
            strtolower(self::DOCUMENT_NAME."s") => $user->getKnowledges(),
        );
    }
    /**
     * Displays a form to create a new Knowledge document.
     *
     * @Route("/private/knowledge/new", name="private_user_knowledge_new")
     * @Template()
     */
    public function newAction()
    {
        return $this->displayNewForm($this->getUser(), self::DOCUMENT_NAME);
    }

    /**
     * Creates a new Knowledge document.
     *
     * @Route("/private/knowledge/create", name="private_user_knowledge_create")
     * @Method("POST")
     * @Template("GreibitBioBundle:Knowledge:new.html.twig")
     *
     */
    public function createAction()
    {
        return $this->makeCreate($this->getUser(), self::DOCUMENT_NAME);
    }

    /**
     * Displays a form to edit an existing Knowledge document.
     *
     * @Route("/private/knowledge/{id}/edit", name="private_user_knowledge_edit")
     * @Template()
     *
     */
    public function editAction($id)
    {
        return $this->displayEditForm($id, $this->getUser(), self::DOCUMENT_NAME);
    }

    /**
     * Edits an existing Knowledge document.
     *
     * @Route("/private/knowledge/{id}/update", name="private_user_knowledge_update")
     * @Method("POST")
     *
     */
    public function updateAction($id)
    {
        return $this->makeUpdate($this->getUser(), $id, self::DOCUMENT_NAME);
    }

    /**
     * Deletes a Knowledge document.
     *
     * @Route("/private/knowledge/{id}/delete", name="private_user_knowledge_delete")
     * @Method("POST")
     *
     */
    public function deleteAction($id)
    {
        return $this->makeDelete($this->getUser(), $id, self::DOCUMENT_NAME);
    }
}
