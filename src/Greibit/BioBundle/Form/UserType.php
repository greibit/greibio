<?php

namespace Greibit\BioBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class UserType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('name')
            ->add('surname')
            ->add('email')
            ->add('telephone')
            ->add('birthday')
            ->add('address')
            ->add('locality')
            ->add('city')
            ->add('nacionality')
        ;
    }

    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'Greibit\BioBundle\Document\User'
        ));
    }

    public function getName()
    {
        return 'user';
    }
}
