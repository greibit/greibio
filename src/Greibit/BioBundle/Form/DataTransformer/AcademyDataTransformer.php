<?php
/**
 * Created by JetBrains PhpStorm.
 * User: Greibit PC
 * Date: 13/10/13
 * Time: 13:18
 * To change this template use File | Settings | File Templates.
 */

namespace Greibit\BioBundle\Form\DataTransformer;
use Greibit\AdminBundle\Document\Academy;
use Symfony\Component\Form\DataTransformerInterface;
use Symfony\Component\Form\Exception\TransformationFailedException;
use Doctrine\Common\Persistence\ObjectManager;

class AcademyDataTransformer implements DataTransformerInterface
{
    /**
     * @var ObjectManager
     */
    private $om;

    /**
     * @param ObjectManager $om
     */
    public function __construct(ObjectManager $om)
    {
        $this->om = $om;
    }

    /**
     * Transforms an object (academy) to a string (number).
     *
     * @param  Academy|null $academy
     * @return string
     */
    public function transform($academy)
    {
        if (null === $academy) {
            return "";
        }

        return $academy->getName();
    }

    /**
     * Transforms a string (name) to an object (academy).
     *
     * @param  string $name
     *
     * @return Academy|null
     *
     * @throws TransformationFailedException if object (academy) is not found.
     */
    public function reverseTransform($name)
    {
        if (!$name) {
            return null;
        }

        $academy = $this->om
            ->getRepository('GreibitAdminBundle:Academy')
            ->findOneBy(array('name' => $name))
        ;

        if (null === $academy) {
            $newAcademy = new Academy();
            $newAcademy->setName($name);
            $this->om->persist($newAcademy);
            $this->om->flush();
        }

        return $academy;
    }
}