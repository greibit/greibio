<?php
namespace Greibit\BioBundle\Form\DataTransformer;
use Greibit\AdminBundle\Document\Academy;
use Greibit\AdminBundle\Document\Company;
use Symfony\Component\Form\DataTransformerInterface;
use Symfony\Component\Form\Exception\TransformationFailedException;
use Doctrine\Common\Persistence\ObjectManager;

class CompanyDataTransformer implements DataTransformerInterface
{
    /**
     * @var ObjectManager
     */
    private $om;

    /**
     * @param ObjectManager $om
     */
    public function __construct(ObjectManager $om)
    {
        $this->om = $om;
    }

    /**
     * Transforms an object ($company) to a string (number).
     *
     * @param  Company|null $company
     * @return string
     */
    public function transform($company)
    {
        if (null === $company) {
            return "";
        }

        return $company->getName();
    }

    /**
     * Transforms a string (name) to an object ($company).
     *
     * @param  string $name
     *
     * @return Company|null
     *
     * @throws TransformationFailedException if object ($company) is not found.
     */
    public function reverseTransform($name)
    {
        if (!$name) {
            return null;
        }

        $company = $this->om
            ->getRepository('GreibitAdminBundle:Company')
            ->findOneBy(array('name' => $name))
        ;

        if (null === $company) {
            $company = new Academy();
            $company->setName($name);
            $this->om->persist($company);
            $this->om->flush();
        }

        return $company;
    }
}