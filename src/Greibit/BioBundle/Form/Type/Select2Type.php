<?php

namespace Greibit\BioBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class Select2Type extends AbstractType {

    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'attr' => array('class' => 'select2')
        ));
    }

    public function getParent()
    {
        return 'document';
    }

    public function getName()
    {
        return 'select2';
    }
}