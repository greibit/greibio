<?php
/**
 * Created by JetBrains PhpStorm.
 * User: Greibit PC
 * Date: 5/10/13
 * Time: 18:36
 * To change this template use File | Settings | File Templates.
 */

namespace Greibit\BioBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;
class DateType extends AbstractType {

    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'format' => 'dd-MM-yyyy',
            'years' => range(1940,2015),
            'widget' => "single_text",
            'attr' => array(
                'class' => 'datepicker'
            )
        ));
    }

    public function getParent()
    {
        return 'date';
    }

    public function getName()
    {
        return 'greibio_date';
    }
}