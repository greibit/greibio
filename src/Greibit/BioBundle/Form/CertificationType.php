<?php

namespace Greibit\BioBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class CertificationType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('name')
            ->add('date', 'greibio_date')
            ->add('academy', "select2", array(
                'class' => 'Greibit\AdminBundle\Document\Academy',
                'property' => 'name'
            ))
        ;
    }

    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'Greibit\BioBundle\Document\Certification'
        ));
    }

    public function getName()
    {
        return 'certification';
    }
}
