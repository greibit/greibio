<?php

namespace Greibit\BioBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class TrainingType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('startDate', 'greibio_date')
            ->add('finishDate', 'greibio_date')
            ->add('title')
            ->add('academy', 'select2', array(
                'class' => 'Greibit\AdminBundle\Document\Academy',
                'property' => 'name'
            ))
        ;
    }

    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'Greibit\BioBundle\Document\Training'
        ));
    }

    public function getName()
    {
        return 'training';
    }
}
