<?php

namespace Greibit\BioBundle\Form;

use Greibit\BioBundle\Form\DataTransformer\CompanyDataTransformer;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class ExperienceType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $entityManager = $options['em'];
        $transformer = new CompanyDataTransformer($entityManager);

        $builder
            ->add('startDate', 'greibio_date')
            ->add('finishDate', 'greibio_date')
            ->add($builder->create('company', 'autocomplete')
                ->addModelTransformer($transformer))
            ->add('work')
            ->add('project')
        ;
    }

    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'Greibit\BioBundle\Document\Experience'
        ));

        $resolver->setRequired(array(
            'em',
        ));

        $resolver->setAllowedTypes(array(
            'em' => 'Doctrine\Common\Persistence\ObjectManager',
        ));
    }

    public function getName()
    {
        return 'experience';
    }
}
