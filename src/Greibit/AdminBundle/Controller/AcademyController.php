<?php

namespace Greibit\AdminBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Greibit\AdminBundle\Document\Academy;
use Greibit\AdminBundle\Form\AcademyType;

/**
 * Academy controller.
 *
 * @Route("/admin/academy")
 */
class AcademyController extends Controller
{
    /**
     * Lists all Academy documents.
     *
     * @Route("/", name="academy")
     * @Template()
     *
     * @return array
     */
    public function indexAction()
    {
        $dm = $this->getDocumentManager();

        $documents = $dm->getRepository('GreibitAdminBundle:Academy')->findAll();

        return array('documents' => $documents);
    }

    /**
     * Displays a form to create a new Academy document.
     *
     * @Route("/new", name="academy_new")
     * @Template()
     *
     * @return array
     */
    public function newAction()
    {
        $document = new Academy();
        $form = $this->createForm(new AcademyType(), $document);

        return array(
            'document' => $document,
            'form'     => $form->createView()
        );
    }

    /**
     * Creates a new Academy document.
     *
     * @Route("/create", name="academy_create")
     * @Method("POST")
     * @Template("GreibitAdminBundle:Academy:new.html.twig")
     *
     * @param Request $request
     *
     * @return array
     */
    public function createAction(Request $request)
    {
        $document = new Academy();
        $form     = $this->createForm(new AcademyType(), $document);
        $form->handlerequest($request);

        if ($form->isValid()) {
            $dm = $this->getDocumentManager();
            $dm->persist($document);
            $dm->flush();

            return $this->redirect($this->generateUrl('academy_show', array('id' => $document->getId())));
        }

        return array(
            'document' => $document,
            'form'     => $form->createView()
        );
    }

    /**
     * Finds and displays a Academy document.
     *
     * @Route("/{id}/show", name="academy_show")
     * @Template()
     *
     * @param string $id The document ID
     *
     * @return array
     *
     * @throws \Symfony\Component\HttpKernel\Exception\NotFoundHttpException If document doesn't exists
     */
    public function showAction($id)
    {
        $dm = $this->getDocumentManager();

        $document = $dm->getRepository('GreibitAdminBundle:Academy')->find($id);

        if (!$document) {
            throw $this->createNotFoundException('Unable to find Academy document.');
        }

        $deleteForm = $this->createDeleteForm($id);

        return array(
            'document' => $document,
            'delete_form' => $deleteForm->createView(),
        );
    }

    /**
     * Displays a form to edit an existing Academy document.
     *
     * @Route("/{id}/edit", name="academy_edit")
     * @Template()
     *
     * @param string $id The document ID
     *
     * @return array
     *
     * @throws \Symfony\Component\HttpKernel\Exception\NotFoundHttpException If document doesn't exists
     */
    public function editAction($id)
    {
        $dm = $this->getDocumentManager();

        $document = $dm->getRepository('GreibitAdminBundle:Academy')->find($id);

        if (!$document) {
            throw $this->createNotFoundException('Unable to find Academy document.');
        }

        $editForm = $this->createForm(new AcademyType(), $document);
        $deleteForm = $this->createDeleteForm($id);

        return array(
            'document'    => $document,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        );
    }

    /**
     * Edits an existing Academy document.
     *
     * @Route("/{id}/update", name="academy_update")
     * @Method("POST")
     * @Template("GreibitAdminBundle:Academy:edit.html.twig")
     *
     * @param Request $request The request object
     * @param string $id       The document ID
     *
     * @return array
     *
     * @throws \Symfony\Component\HttpKernel\Exception\NotFoundHttpException If document doesn't exists
     */
    public function updateAction(Request $request, $id)
    {
        $dm = $this->getDocumentManager();

        $document = $dm->getRepository('GreibitAdminBundle:Academy')->find($id);

        if (!$document) {
            throw $this->createNotFoundException('Unable to find Academy document.');
        }

        $deleteForm = $this->createDeleteForm($id);
        $editForm   = $this->createForm(new AcademyType(), $document);
        $editForm->handlerequest($request);

        if ($editForm->isValid()) {
            $dm->persist($document);
            $dm->flush();

            return $this->redirect($this->generateUrl('academy_edit', array('id' => $id)));
        }

        return array(
            'document'    => $document,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        );
    }

    /**
     * Deletes a Academy document.
     *
     * @Route("/{id}/delete", name="academy_delete")
     * @Method("POST")
     *
     * @param Request $request The request object
     * @param string $id       The document ID
     *
     * @return array
     *
     * @throws \Symfony\Component\HttpKernel\Exception\NotFoundHttpException If document doesn't exists
     */
    public function deleteAction(Request $request, $id)
    {
        $form = $this->createDeleteForm($id);
        $form->handlerequest($request);

        if ($form->isValid()) {
            $dm = $this->getDocumentManager();
            $document = $dm->getRepository('GreibitAdminBundle:Academy')->find($id);

            if (!$document) {
                throw $this->createNotFoundException('Unable to find Academy document.');
            }

            $dm->remove($document);
            $dm->flush();
        }

        return $this->redirect($this->generateUrl('academy'));
    }

    private function createDeleteForm($id)
    {
        return $this->createFormBuilder(array('id' => $id))
            ->add('id', 'hidden')
            ->getForm()
        ;
    }

    /**
     * Returns the DocumentManager
     *
     * @return DocumentManager
     */
    private function getDocumentManager()
    {
        return $this->get('doctrine.odm.mongodb.document_manager');
    }
}
