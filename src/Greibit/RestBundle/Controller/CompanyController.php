<?php
namespace Greibit\RestBundle\Controller;

use FOS\RestBundle\Controller\FOSRestController;

class CompanyController extends FOSRestController
{
    public function getCompaniesAction()
    {
        $data = $this->getCompanyRepository()->findAll();
        return $this->handleView($this->view($this->toArray($data)));
    }

    private function toArray($array){
        $salida = array();
        foreach($array as $i)
            $salida[] = $i;

        return $salida;
    }

    public function getCompanyRepository(){
        return $this->get('doctrine.odm.mongodb.document_manager')->getRepository("GreibitAdminBundle:Company");
    }
}