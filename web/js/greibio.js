$(function(){
    $(document).on("focus",".datepicker", function(){
        $(this).datepicker({"format" : "dd-mm-yyyy"});
    });

    function toggleModal(){
        $modalWindow.modal('toggle');
    }

    function ajaxLoadData(entity){
        $element = $("#"+entity+"-data");
        startLoading("data", $element[0]);

        $element.load($element.data("href"),
            function(){
                stopLoading("data");
            }
        );
    }

    var iconClickModalFunction = function(e){
        e.preventDefault();

        startIconLoading($(this));

        $modalContent.load($(this).attr("href"), function(response, status, xhr){
            if ( status == "error" ) {
                console.dir(response);
                ErrorAlert(response.message);
            }else{
                $modalWindow.modal('toggle');
                stopIconLoading();
            }
        });
    }

    var submitModalFormFunction = function(e){
        e.preventDefault();

        $formElement = $(this);
        startLoading("form", $formElement[0]);

        var ajax = $.post(
            $(this).attr('action'),
            $(this).serialize()
        );
        ajax.done(function(response){
            SuccessAlert(response.message);

            toggleModal();
            ajaxLoadData($formElement.data("entity"));
        });
        ajax.fail(function(response){
            //Tratar errores de formulario recibidos
            ErrorAlert("Ha habido errores en el envío");
        });
        ajax.complete(function(){
            stopLoading("form");
        });
    }

    var iconClickDeleteFunction = function(e){
        e.preventDefault();

        var entity = $(this).data("entity");
        startIconLoading($(this));

        var ajax = $.post(
            $(this).attr('href')
        );
        ajax.done(function(response){
            SuccessAlert(response.message);
            ajaxLoadData(entity);
        });
        ajax.fail(function(response){
            //Tratar errores de formulario recibidos
            ErrorAlert("Ha habido errores en el envío");
        });
        ajax.complete(function(){
            stopIconLoading();
        });
    }

    $(document).on("click", ".edit-link", iconClickModalFunction);

    $(document).on("submit", "form.modal-form", submitModalFormFunction);

    $(document).on("click", ".new-form", iconClickModalFunction);

    $(document).on("click", ".delete-link", iconClickDeleteFunction);
});
